import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:studyblocs/app/app.dart';
import 'package:path_provider/path_provider.dart';

void main() async {
   //for async main()
  WidgetsFlutterBinding.ensureInitialized();
  
  //DB INIT
  final appDocumentDir = await getApplicationDocumentsDirectory();
  Hive.init(appDocumentDir.path);
  await Hive.openBox('appDB'); 
  runApp(App());

}


