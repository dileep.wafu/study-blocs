import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:studyblocs/app/blocs/auth_bloc.dart';
import 'package:studyblocs/app/models/user_model.dart';
import 'package:studyblocs/app/ui/home_page.dart';
import 'package:studyblocs/app/ui/landing_page.dart';
import 'package:studyblocs/app/ui/theme_data.dart';



class AuthRouter extends StatefulWidget {
  static Widget create({@required BuildContext context}) {
    return Provider<AuthBloc>(
      create: (_) => AuthBloc(),
      child: AuthRouter(),
      dispose: (context, bloc) => bloc.dispose(),
    );
  }

  @override
  _AuthRouterState createState() => _AuthRouterState();
}

class _AuthRouterState extends State<AuthRouter> {
  @override
  Widget build(BuildContext context) {
    final authBloc = Provider.of<AuthBloc>(context);

    return MaterialApp(
      title: 'Study Bloc',
      debugShowCheckedModeBanner: false,
      theme: themeData,
      home: StreamBuilder(
          stream: authBloc.authStream,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.active) {
              User user = snapshot.data;
              if (user == null) {
                return LandingPage();
              }
              return HomePage();
            } else {
              return Scaffold(
                body: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          }),
    );

    ;
  }
}
