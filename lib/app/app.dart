import 'package:flutter/material.dart';
import 'package:studyblocs/app/routers/auth_router.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return AuthRouter.create(context: context);
  
  }
}
