import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:studyblocs/app/models/revision_schedule_model.dart';

class DatabaseService {
  Box _box;
  String uid;
  final Firestore _db = Firestore.instance;

  /// Initialize the userID
  DatabaseService() {
    _box = Hive.box('appDB');
    uid = _box.get('uid');
    // print("@@DB SERVICE UID :: $uid");
  }

  Stream<List<RevisionScheduleModel>> revisionSchedulesStream() {
    var startDate =
        DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
    var endDate = DateTime(startDate.year, startDate.month, startDate.day + 1);

    //   final startDate = DateTime.parse('2020-03-10');
    // final endDate = DateTime.parse('2020-03-11');

    var snaps = _db
        .collection(uid)
        .document('userDB')
        .collection('revisionSchedules')
        .where('scheduledAt', isGreaterThanOrEqualTo: startDate)
        .where('scheduledAt', isLessThan: endDate)
        .snapshots();
    return snaps.map((list) => list.documents
        .map((doc) => RevisionScheduleModel.fromFirestore(doc))
        .toList());
  }

  Stream<List<RevisionScheduleModel>> revisionSchedulesByDateStream(
      DateTime startDate) {
    // var startDate =
    //     DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
    var endDate = DateTime(startDate.year, startDate.month, startDate.day + 1);

    //   final startDate = DateTime.parse('2020-03-10');
    // final endDate = DateTime.parse('2020-03-11');

    var snaps = _db
        .collection(uid)
        .document('userDB')
        .collection('revisionSchedules')
        .where('scheduledAt', isGreaterThanOrEqualTo: startDate)
        .where('scheduledAt', isLessThan: endDate)
        .snapshots();
    return snaps.map((list) => list.documents
        .map((doc) => RevisionScheduleModel.fromFirestore(doc))
        .toList());
  }

  addnewRevision(
      {@required RevisionScheduleModel rev,
      @required List<int> revSeqs}) async {
    DateTime scheduledDate =
        DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

    try {
      for (int seq in revSeqs) {
        scheduledDate = DateTime(
            scheduledDate.year, scheduledDate.month, scheduledDate.day + seq);
        var ts = Timestamp.fromDate(scheduledDate);
        rev.scheduledAt = ts;
        // print(rev.toJson());
        // print(scheduledDate);
        // print('\n\n');
        await addRevisionScheduletoDB(rev.toJson());
      }
    } catch (e) {
      rethrow;
    }
  }

  addRevisionScheduletoDB(data) async {
    try {
      await _db
          .collection(uid)
          .document('userDB')
          .collection('revisionSchedules')
          .add(data);
    } catch (e) {
      rethrow;
    }
  }

  updateRevisionSchedule({@required RevisionScheduleModel res}) async {
    try {
      await _db
          .collection(uid)
          .document('userDB')
          .collection('revisionSchedules')
          .document(res.id)
          .updateData(res.toJson());
    } catch (e) {}
  }

  deleteRevisionSchedule({@required RevisionScheduleModel res}) async {
    try {
      await _db
          .collection(uid)
          .document('userDB')
          .collection('revisionSchedules')
          .document(res.id)
          .delete();
    } catch (e) {
      rethrow;
    }
  }

  deleteRevisionSchedulesCurrentFollow(
      {@required RevisionScheduleModel res}) async {
    // delete current and following
    try {
      await _db
          .collection(uid)
          .document('userDB')
          .collection('revisionSchedules')
          .where('scheduledAt',
              isGreaterThanOrEqualTo: res.scheduledAt.toDate())
          .where('commonId', isEqualTo: res.commonId)
          .getDocuments()
          .then((snapshot) {
        for (DocumentSnapshot doc in snapshot.documents) {
          doc.reference.delete();
          // print('${doc.data['topic']} ++ ${doc.documentID} ++ "commonid" ${doc.data['commonId']}');
        }
      });
    } catch (e) {
      rethrow;
    }
  }
}
