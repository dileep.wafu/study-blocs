import 'dart:async';

import 'package:hive/hive.dart';
import 'package:studyblocs/app/models/user_model.dart';
import 'package:studyblocs/app/services/auth_service.dart';

class AuthBloc {
  final authService = AuthService();
  bool isLoading = false;
  String _userId = '';


  /// Streams
  final StreamController<bool> _isLoadingController = StreamController<bool>();

  Stream<bool> get isLoadingStream => _isLoadingController.stream;
  Stream<User> get authStream => authService.onAuthStateChanged;

  String get userId => _userId;

  void dispose() {
    _isLoadingController.close();
  }

  void _setIsLoading(bool isLoading) {
    this.isLoading = isLoading;
    _isLoadingController.add(isLoading);
  }

  Future<User>  currentUser() async{
    return await authService.currentUser();
  }

  Future<User> signInWithGoogle() async {
    try {
      _setIsLoading(true);
      User user = await authService.signInWithGoogle();
      _userId = user.uid;
      
      return await authService.signInWithGoogle();
    } catch (e) {
      _setIsLoading(false);
      rethrow;
    }
  }

  Future<void> signOut() async {
    _userId = '';
    await authService.signOut();
  }
}
