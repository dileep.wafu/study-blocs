

import 'package:cloud_firestore/cloud_firestore.dart';

class RevisionScheduleModel {
  String id;
  String commonId;
  bool isCompleted;
  String note;
  Timestamp scheduledAt;
  String subjectCode;
  String topic;

  RevisionScheduleModel(
      {this.id,
      this.isCompleted,
      this.note,
      this.scheduledAt,
      this.subjectCode,
      this.topic, this.commonId});

  RevisionScheduleModel.fromFirestore(DocumentSnapshot json) {
    id = json.documentID;
    isCompleted = json.data['isCompleted'];
    note = json.data['note'];
    scheduledAt = json.data['scheduledAt'];
    subjectCode = json.data['subjectCode'];
    topic = json.data['topic'];
    commonId = json.data['commonId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    // data['id'] = this.id;
    data['isCompleted'] = this.isCompleted;
    data['note'] = this.note;
    data['scheduledAt'] = this.scheduledAt;
    data['subjectCode'] = this.subjectCode;
    data['topic'] = this.topic;
    data['commonId'] = this.commonId;
    return data;
  }
}
