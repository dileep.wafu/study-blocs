import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:studyblocs/app/blocs/auth_bloc.dart';
import 'package:studyblocs/app/ui/revision_schedule_page/revision_schedule_page.dart';

class MyDrawer extends StatefulWidget {
  final AuthBloc authBloc;

  const MyDrawer({Key key, @required this.authBloc}) : super(key: key);

  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {



  @override
  Widget build(BuildContext context) {

    

 
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Center(
              child: ListTile(
                title: Text('Logout'),
                onTap: () async {
                  Navigator.of(context).pop();
                  await widget.authBloc.signOut();
                },
              ),
            ),
          ),
          ListTile(
            title: Text('Revision Schedules'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (cxt) => RevisionSchedulePage(
                        authBloc: widget.authBloc,
                      )));
            },
          ),
        ],
      ),
    );
  }
}
