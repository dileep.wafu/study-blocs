

import 'package:flutter/material.dart';

final themeData = ThemeData(
    // Define the default brightness and colors.
    brightness: Brightness.dark,
    primaryColor: Colors.lightBlue,
    accentColor: Colors.orange,
    buttonColor: Colors.lightBlue,
    
    // floatingActionButtonTheme: FloatingActionButtonThemeData(backgroundColor: ),

    // Define the default font family.
    // fontFamily: 'Georgia',
    
    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    textTheme: TextTheme(
      headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
      title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
      body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
    ),
  );


// final customColor = Colors.deepPurple;