import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:studyblocs/app/blocs/auth_bloc.dart';
import 'package:studyblocs/app/ui/drawer.dart';
import 'package:studyblocs/app/ui/revision_schedule_page/revision_schedule_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey _menuKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    final authBloc = Provider.of<AuthBloc>(context);

    final Widget popupbutton = PopupMenuButton(
        key: _menuKey,
        itemBuilder: (_) => <PopupMenuItem<String>>[
              PopupMenuItem<String>(
                  child: const Text('Logout'), value: 'logout'),
              // new PopupMenuItem<String>(
              //     child: const Text('Lion'), value: 'Lion'),
            ],
        onSelected: (val) {
          if (val == 'logout') authBloc.signOut();
        });

    return Scaffold(
      appBar: AppBar(
        title: Text("Dileep's App"),
        actions: <Widget>[popupbutton],
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 50.0),
            RaisedButton(
              child: Text("Revision Schedules"),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (cxt) => RevisionSchedulePage(
                      authBloc: authBloc,
                    ),
                  ),
                );
              },
            ),
            SizedBox(height: 20.0),

            RaisedButton(
              child: Text("Topic Blocs"),
              onPressed: () {},
            ),
            SizedBox(height: 20.0),

            RaisedButton(
              child: Text("Remember Stuff"),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
