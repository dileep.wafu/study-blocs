import 'package:flutter/material.dart';
import 'package:studyblocs/app/models/revision_schedule_model.dart';
import 'package:studyblocs/app/services/db_service.dart';
import 'package:uuid/uuid.dart';

class AddRevisionPage extends StatefulWidget {
  @override
  _AddRevisionPageState createState() => _AddRevisionPageState();
}

class _AddRevisionPageState extends State<AddRevisionPage> {
  final DatabaseService db = DatabaseService();

  RevisionScheduleModel rev = RevisionScheduleModel();
  String subjectCode = '';
  String topicName = '';
  String note = '';
  List<int> revisionSeq = [];
  String revisionSeqString = '';
  List<String> subjectCodes = [
    'Select Subject Code',
    'DS',
    'ALGO',
    'TOC',
    'CD',
    'DLD',
    'CN',
    'OS',
    'DB',
    'CO',
    'DM',
    'GT',
    'EM',
    'APT'
  ];

  bool isLoading = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    rev.commonId = Uuid().v1();
    rev.isCompleted = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        key: _scaffoldKey,
        appBar: _buildAppBar(context),
        body: _buildPage(),
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
      title: Text('Add a Revision Schedule'),
      automaticallyImplyLeading: false,
      actions: <Widget>[
        new IconButton(
          icon: new Icon(Icons.close),
          onPressed: () => Navigator.of(context).pop(null),
        ),
      ],
    );
  }

  _buildPage() {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 20.0),
          _subjectCodeField(),
          SizedBox(height: 20.0),
          _topicNameField(),
          SizedBox(height: 20.0),
          _noteField(),
          SizedBox(height: 30.0),
          _revisionSequence(),
          SizedBox(height: 30),
          _buildAddButton(),
        ],
      ),
    );
  }


  /// Components

  _subjectCodeField() {
    return DropdownButton<String>(
      isExpanded: true,
      value: subjectCode == '' ? 'Select Subject Code' : subjectCode,
      items: subjectCodes.map((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
      onChanged: (val) {
        setState(() {
          // print(val);
          if (val != 'Select Subject Code')
            this.subjectCode = val;
          else
            this.subjectCode = '';
        });
      },
    );
  }

  _topicNameField() {
    return TextField(
      onChanged: (val) {
        topicName = val;
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Topic Name',
      ),
    );
  }

  _noteField() {
    return TextField(
      maxLines: 5,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Note or Description',
      ),
      onChanged: (val) {
        setState(() {
          this.note = val;
        });
      },
    );
  }

  _revisionSequence() {
    return TextField(
      maxLines: 5,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Enter revicion sequence \nEx: 7,7,15,30,30,30',
      ),
      onChanged: (val) {
        setState(() {
          revisionSeqString = val;
        });
      },
    );
  }

  Widget _buildAddButton() {
    return isLoading == true? CircularProgressIndicator(): RaisedButton(
      color: Colors.blue,
      textTheme: ButtonTextTheme.primary,
      onPressed: () {
        submitForm();
      },
      child: Text(
        'Add',
        style: TextStyle(
          fontFamily: 'Montserrat',
          fontSize: 16.0,
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  /// Form submissions
  submitForm() async {
    if (validateForm() == true) {
      setState(() {
        isLoading = true;
      });
      rev.note = note;
      rev.subjectCode = subjectCode;
      rev.topic = topicName;
      // print("FORM IS VALID ::: ${rev.toJson()}");
      // print("SEQUENCES :: $revisionSeq");

      try {

        await db.addnewRevision(rev: rev, revSeqs: revisionSeq);
        Navigator.of(context).pop();
        setState(() {
          isLoading = false;
        });
      } catch (e) {
            setState(() {
          isLoading = false;
        });
        showInSnackBar(e);
      }
    }
  }

  /// Validations of Form

  bool validateForm() {
    if (subjectCode == '' ||
        topicName == '' ||
        note == '' ||
        validateRevisionSeq() == false) {
      showInSnackBar('Please enter valid info');
      return false;
    } else
      return true;
  }

  bool validateRevisionSeq() {
    bool isValid = true;
    List<String> vals = revisionSeqString.split(',');
    for (var val in vals) {
      if (parseit(val) == false) {
        isValid = false;
        revisionSeq.clear();
        break;
      }
    }
    // print(" VALIDITY ::: $isValid");
    return isValid;
  }

  bool parseit(val) {
    try {
      int parsedVal = int.parse(val);
      setState(() {
        revisionSeq.add(parsedVal);
      });
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  void showInSnackBar(String msg) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(msg)));
  }

}
