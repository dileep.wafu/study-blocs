import 'package:flutter/material.dart';
import 'package:studyblocs/app/models/revision_schedule_model.dart';
import 'package:studyblocs/app/ui/theme_data.dart';

class CustomFloatingBtn extends StatelessWidget {
  
  final Function(RevisionScheduleModel) onPressed;
  final RevisionScheduleModel res;
  const CustomFloatingBtn({Key key, @required this.onPressed, @required this.res}) : super(key: key); 
  
  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      
      shape: StadiumBorder(),
      onPressed: onPressed(res),
      fillColor: Theme.of(context).primaryColor,
      splashColor: Theme.of(context).primaryColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              Icons.check,
              color: Colors.white,
            ),
            SizedBox(width: 8.0),
            Text(
              "Mark complete",
              style: TextStyle(
                color: Colors.white
              ),
            ),
            
          ],
        ),
      ),
    );
  }
}