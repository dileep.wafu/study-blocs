import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:studyblocs/app/blocs/auth_bloc.dart';
import 'package:studyblocs/app/models/revision_schedule_model.dart';
import 'package:studyblocs/app/services/db_service.dart';
import 'package:studyblocs/app/ui/revision_schedule_page/add_revision_page.dart';
import 'package:studyblocs/app/ui/revision_schedule_page/view_revision_page.dart';
import 'package:table_calendar/table_calendar.dart';

class RevisionSchedulePage extends StatefulWidget {
  final AuthBloc authBloc;

  const RevisionSchedulePage({Key key, @required this.authBloc})
      : super(key: key);

  @override
  _RevisionSchedulePageState createState() => _RevisionSchedulePageState();
}

class _RevisionSchedulePageState extends State<RevisionSchedulePage> {
  AuthBloc authBloc;
  final DatabaseService db = DatabaseService();
  CalendarController _calendarController;
  final GlobalKey _menuKey = new GlobalKey();

  DateTime date =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

  @override
  void didChangeDependencies() {
    authBloc = Provider.of<AuthBloc>(context);
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Widget popupbutton = PopupMenuButton(
        key: _menuKey,
        itemBuilder: (_) => <PopupMenuItem<String>>[
              PopupMenuItem<String>(
                  child: const Text('Logout'), value: 'logout'),
              // new PopupMenuItem<String>(
              //     child: const Text('Lion'), value: 'Lion'),
            ],
        onSelected: (val) {
          if (val == 'logout') authBloc.signOut();
        });

    return Scaffold(
      appBar: AppBar(
        title: Text('Scheduled Revision'),
        actions: <Widget>[popupbutton],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (ctx) => AddRevisionPage()));
        },
        child: Icon(Icons.add),
      ),
      body: _buildPage(),
    );
  }

  _buildPage() {
    return SafeArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 10.0),
          TableCalendar(
            calendarStyle: CalendarStyle(
                todayColor: Theme.of(context).accentColor,
                selectedColor: Theme.of(context).primaryColor),
            headerStyle: HeaderStyle(
              leftChevronIcon: Icon(
                Icons.chevron_left,
                color: Colors.white,
              ),
              centerHeaderTitle: true,
              rightChevronIcon: Icon(
                Icons.chevron_right,
                color: Colors.white,
              ),
              formatButtonVisible: false,
            ),
            daysOfWeekStyle: DaysOfWeekStyle(
              weekdayStyle: TextStyle(color: Colors.white),
              // weekendStyle: TextStyle(color: Theme.of(context).primaryColor),

            ),
      
            calendarController: _calendarController,
            initialCalendarFormat: CalendarFormat.week,
            onDaySelected: (x, y) {
              x = DateTime(x.year, x.month, x.day);
              // print(x);
              setState(() {
                date = x;
              });
            },
          ),
          SizedBox(height: 20.0),
          Expanded(
            flex: 1,
            child: StreamBuilder<List<RevisionScheduleModel>>(
              stream: db.revisionSchedulesByDateStream(date),
              builder: (ctx, snap) {
                if (snap.hasData &&
                    snap.connectionState == ConnectionState.active) {
                  if (snap.data.length == 0)
                    return Text('NO Schedules Planned');
                  else {
                    List<RevisionScheduleModel> data = _sortData(snap.data);

                    return ListView.builder(
                      itemCount: snap.data.length,
                      itemBuilder: (ctx, index) {
                        return _buildScheduleItem(data[index]);
                      },
                    );
                  }
                } else if (snap.hasError) {
                  print("###### ERROR :: ${snap.error}");
                  return Text("ERROR Occured");
                } else {
                  return ListView(
                    children: <Widget>[LinearProgressIndicator()],
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildScheduleItem(RevisionScheduleModel res) {
    // return Text(res.topic);
    return ListTile(
      leading: Checkbox(
        activeColor: Theme.of(context).primaryColor,
        value: res.isCompleted,
        onChanged: (val) {
          RevisionScheduleModel newres = res;
          newres.isCompleted = val;
          db.updateRevisionSchedule(res: newres);
        },
      ),
      title: Text(res.topic),
      subtitle: Text(res.subjectCode.toString()),
      trailing: Icon(Icons.edit),
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (ctx) => ViewRevision(
                  res: res,
                )));
      },
    );
  }

  List<RevisionScheduleModel> _sortData(List<RevisionScheduleModel> lis) {
    List<RevisionScheduleModel> completed = [];
    List<RevisionScheduleModel> inCompleted = [];

    for (RevisionScheduleModel res in lis) {
      if (res.isCompleted == true)
        completed.add(res);
      else
        inCompleted.add(res);
    }

    return [...inCompleted, ...completed];
  }
}
