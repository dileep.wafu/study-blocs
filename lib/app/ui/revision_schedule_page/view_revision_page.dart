import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:studyblocs/app/models/revision_schedule_model.dart';
import 'package:intl/intl.dart';
import 'package:studyblocs/app/services/db_service.dart';
import 'package:studyblocs/app/ui/revision_schedule_page/helpers/custom_floating_action_btn.dart';
import 'package:studyblocs/app/ui/theme_data.dart';


class ViewRevision extends StatefulWidget {
  final RevisionScheduleModel res;

  const ViewRevision({Key key, @required this.res}) : super(key: key);
  @override
  _ViewRevisionState createState() => _ViewRevisionState();
}

class _ViewRevisionState extends State<ViewRevision> {
  var formatter = DateFormat('dd-MM-yyyy');
  DatabaseService db = DatabaseService();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: ThemeData.dark().scaffoldBackgroundColor,
          actions: <Widget>[
            GestureDetector(
              onTap: () {
                showDialog(context: context, builder: (_) => _buildDialog());
              },
              child: Icon(Icons.delete_outline),
            ),
            SizedBox(width: 20.0)
          ],
        ),
        body: _buildPage(),
        floatingActionButton: CustomFloatingBtn(
          res: widget.res,
          onPressed: (res) {
            print(res.topic);
          },
        ));
  }

  _buildPage() {
    return ListView(
      children: <Widget>[
       if(isLoading==true) LinearProgressIndicator(),

        Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

               Text(
            widget.res.subjectCode,
            style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 16.0),
          ),
          SizedBox(height: 10.0),
          Text(
            widget.res.topic,
            style: TextStyle(
                color: Colors.white,
                fontSize: 34.0,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 25.0),
          Row(
            children: <Widget>[
              Icon(
                Icons.description,
                size: 32.0,
              ),
              SizedBox(
                width: 10.0,
              ),
              Text(
                widget.res.note,
                style: TextStyle(fontSize: 20.0),
              )
            ],
          ),
          SizedBox(height: 20.0),
          Row(
            children: <Widget>[
              Icon(Icons.date_range, size: 32.0),
              SizedBox(
                width: 10.0,
              ),
              Text(formatter.format(widget.res.scheduledAt.toDate()),
                  style: TextStyle(fontSize: 20.0))
            ],
          )
            ],
          ),
        )
       
      ],
    );
  }

  _buildDialog() {
    return AlertDialog(
      title: Text("Delete Event"),
      content: Text('What you want to delete ?'),
      actions: isLoading == true
          ? [loading()]
          : [
              deleteThisBtn(),
              deletehisAndFollowBtn(),
              cancelBtn(),
            ],
    );
  }

  Widget loading() {
    return Text('Loading...');
  }

  // set up the buttons
  Widget deletehisAndFollowBtn() => FlatButton(
        child: Text("Delete this and following"),
        onPressed: () async {
          Navigator.of(context).pop();

          setState(() {
            isLoading = true;
          });
          await db.deleteRevisionSchedulesCurrentFollow(res: widget.res);
          Navigator.of(context).pop();

        },
      );
  Widget cancelBtn() => FlatButton(
        child: Text("Cancel"),
        onPressed: () {
          Navigator.of(context).pop();
        },
      );
  Widget deleteThisBtn() => FlatButton(
        child: Text("Delete this"),
        onPressed: () async {
          Navigator.of(context).pop();

          setState(() {
            isLoading = true;
          });
          await db.deleteRevisionSchedule(res: widget.res);
          Navigator.of(context).pop();

        },
      );
}
