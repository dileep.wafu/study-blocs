import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:studyblocs/app/blocs/auth_bloc.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authBloc = Provider.of<AuthBloc>(context);

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(onPressed: () async{
              await authBloc.signInWithGoogle();
            },
            child: Text("Login With Google"),
            ),

            StreamBuilder(
              stream: authBloc.isLoadingStream,
              initialData: false,
              builder: (ctx,snap){
                if(snap.hasData && snap.data==true) return CircularProgressIndicator();
                else return SizedBox();
              }
              )
          ],
        ),
      ),
      
    );
  }
}